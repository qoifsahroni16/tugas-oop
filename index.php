<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Name : ".$sheep->name. "<br>";// "shaun"
echo "Legs  : ".$sheep->legs. "<br>"; // 4
echo "cold blooded : ". $sheep->cold_blooded. "<br> <br>"; // "no"

$frog = new Frog ("buduk");

echo "Name : ".$frog->name. "<br>";
echo "Legs  : ".$frog->legs. "<br>"; 
echo "cold blooded : ".$frog->cold_blooded. "<br>"; 
echo $frog ->jump();


$ape = new Ape ("kera sakti");

echo "Name : ".$ape->name. "<br>";
echo "Legs  : ".$ape->legs. "<br>"; 
echo "cold blooded : ".$ape->cold_blooded. "<br>"; 
echo $ape ->Yell();


?>